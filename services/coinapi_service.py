"""
This module contains services for interacting with the CoinAPI.
It includes functions to get all assets, get exchange rates, and plot rates.
"""

# !/usr/bin/env python
# coding=UTF-8

# pylint: disable=import-error
import logging
from datetime import date, timedelta

import requests
import pandas as pd
import matplotlib.pyplot as plt

from config.coinapi_config import CoinAPIConfig


class CoinAPIService:
    """
    This class contains services for interacting with the CoinAPI.
    """
    def __init__(self):
        self.main_tokens = ['BTC', 'ETH', 'WLD', 'DOT', 'EGLD', 'CHZ', 'BNB']
        self.payload: dict = {}
        self.headers: {str} = {
            'Accept': 'text/plain',
            'X-CoinAPI-Key': CoinAPIConfig.API_KEY_VALUE
            }

    def __str__(self):
        return "CoinAPIService"

    # def coinapi_service_get_all_assets(self):
    #     """
    #     Get all assets from CoinAPI
    #     :return: list of all assets
    #     """
    #     try:
    #         url: str = f"{CoinAPIConfig.BASE_URL}/v1/assets"
    #         response = requests.request("GET", url, headers=self.headers, data=self.payload)
    #         requests_remaining: str = response.headers['x-ratelimit-remaining']
    #         logging.info("Request done successfully.")
    #         logging.info("Remaining Requests : %s", requests_remaining)
    #         # pprint(response.json())
    #         print(f"\n\tNombre de requêtes restantes : {requests_remaining}\n")
    #         return response.json()
    #     except requests.exceptions.RequestException as e:
    #         logging.error("Error: %s", e)
    #         return None

    # def get_all_assets_dict() -> dict:
    #     """
    #     Get all selected assets (see main_tokens) from CoinAPI
    #     :return: dict of all assets
    #     """
    #     # all_assets_dict = {}
    #     # for asset in coinapi_service_get_all_assets():
    #     #     all_assets_dict[asset.get('asset_id')] = asset.get('name')
    #     resp = coinapi_service_get_all_assets()
    #     all_assets_dict = {asset.get('asset_id'): asset.get('name') for asset in resp if
    #                        asset.get('asset_id') in main_tokens}
    #     print('Main Tokens :\n', all_assets_dict)
    #     return all_assets_dict

    # def coin_api_get_exchange_rates_v1() -> dict | None:
    #     """
    #     Get exchange rates from CoinAPI
    #     :return: response.json() of exchange rates
    #     """
    #     url = CoinAPIConfig.BASE_URL + (
    #         "/v1/exchangerate/BTC/EUR/history?period_id=1DAY&time_start=2024-01-01T00:00:00"
    #         "&time_end=2024-01-31T00:00:00")
    #     try:
    #         response = requests.request("GET", url, headers=headers)
    #         pprint(response.json())
    #         logging.info("Request done successfully.")
    #         return response.json()
    #     except requests.exceptions.RequestException as e:
    #         logging.error("Error: %s", e)
    #         return None

    @staticmethod
    def get_date_today() -> date:
        """
        Get the current date
        :return: date
        """
        return date.today()

    def coin_api_get_exchange_rates(self,
                                    asset1: str = 'ETH',
                                    asset2: str = 'EUR',
                                    period_id: str = '1DAY',
                                    time_start: str = (get_date_today() - timedelta(days=30))
                                    .strftime("%Y-%m-%d"),
                                    time_end: str = get_date_today().strftime("%Y-%m-%d"),
                                    ):
        """
        Get exchange rates from CoinAPI
        :param asset1:
        :param asset2:
        :param period_id:
        :param time_start:
        :param time_end:
        :return: exchange_rates_dict
        """
        url = CoinAPIConfig.BASE_URL + (
            f"/v1/exchangerate/{asset1}/{asset2}/history?period_id={period_id}"
            f"&time_start={time_start}T00:00:00"
            f"&time_end={time_end}T00:00:00")

        exchange_rates_dict = None

        try:
            response = requests.request("GET", url, headers=self.headers).json()
            # print("")

            exchange_rates_dict = \
                {rate['time_period_start'].split('.')[0]: round(rate['rate_close'],
                                                                2) for rate in response}
            return exchange_rates_dict  # return the dictionary if the request is successful
        except requests.exceptions.RequestException as e:
            logging.error("Error: %s", e)
            return None

    @staticmethod
    def create_dataframe(rates_dict) -> pd.DataFrame | None:
        """
        Create a dataframe from the exchange rates
        :param rates_dict:
        :return: df
        """
        try:
            df = pd.DataFrame(rates_dict.items(), columns=['Date', 'Rate'])
            # pprint(df)
            return df
        except ValueError as e:
            logging.error("Error: %s", e)
            return None
        except TypeError as e:
            logging.error("Error: %s", e)
            return None
        except requests.exceptions.RequestException as e:
            logging.error("Error: %s", e)
            return None

    @staticmethod
    def plot_rates(df, token1='ETH', token2='EUR'):
        """
        Plot exchange rates
        :param df:
        :param token1: str
        :param token2: str
        """
        try:
            # Créer une figure et un axe
            fig, ax = plt.subplots()

            # Convertir les dates en format datetime pour un meilleur affichage
            df['Date'] = pd.to_datetime(df['Date'])

            # Créer la courbe avec une couleur spécifique
            ax.plot(df['Date'], df['Rate'], color='cyan')   # Change color here

            # Ajouter un point rouge à la dernière valeur de la courbe
            ax.scatter(df['Date'].iloc[-1], df['Rate'].iloc[-1], color='red')

            # Ajouter une annotation avec la valeur à côté (à gauche) du point rouge
            ax.text(df['Date'].iloc[-1] - pd.Timedelta(days=3),
                    df['Rate'].iloc[-1],
                    f' {df["Rate"].iloc[-1]}',
                    color='cyan')  # couleur du texte

            # Ajouter des lignes verticales pointillées à chaque date
            for d in df['Date']:
                ax.axvline(x=d, color='gray', linestyle='dotted')

            # Ajuster les étiquettes de l'axe des x pour qu'elles apparaissent verticalement
            plt.xticks(rotation='vertical', color='blue')

            # Ajuster les étiquettes de l'axe des x pour qu'elles apparaissent verticalement
            plt.yticks(rotation='horizontal', color='orange')

            # Ajouter des lignes de grille
            ax.grid(True)

            # Changer la couleur de fond en noir
            ax.set_facecolor('black')

            # Définir les titres des axes et du graphique
            plt.xlabel('Date', color='blue')
            plt.ylabel('Rate', color='green')
            plt.title(f'Cours du {token1} (en {token2}) '
                      f'entre le {df["Date"].min().strftime("%Y-%m-%d")} et le '
                      f'{df["Date"].max().strftime("%Y-%m-%d")}', color='red')

            # Afficher le graphique
            plt.show()

        except ValueError as e:
            logging.error("Error: %s", e)
            return None
        except TypeError as e:
            logging.error("Error: %s", e)
            return None
        except requests.exceptions.RequestException as e:
            logging.error("Error: %s", e)
            return None
