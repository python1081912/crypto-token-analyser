"""
This module contains the configuration for CoinAPI.
"""

# !/usr/bin/env python
# coding=UTF-8

import os


# pylint: disable=too-few-public-methods
class CoinAPIConfig:
    """
    CoinAPI Configuration
    """
    BASE_URL: str = "https://rest.coinapi.io"
    API_KEY_VALUE: str = os.getenv('COINAPI_API_KEY')
