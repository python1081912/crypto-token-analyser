"""
This is the main module of the application. It imports functions from the coinapi_service module
to fetch exchange rates for a given pair of assets over a specified period, create a dataframe from the fetched data,
and plot the rates. The main function of this module is to call these functions with specified parameters.
"""

# !/usr/bin/env python
# coding=UTF-8

import argparse
from datetime import timedelta, date

from services.coinapi_service import CoinAPIService


def main():
    coinapi_service = CoinAPIService()

    parser = argparse.ArgumentParser(description='Analyse des tokens')
    parser.add_argument('--token1', type=str, required=True, help='Le premier token à analyser')
    parser.add_argument('--token2', type=str, required=True, help='Le deuxième token à analyser')

    args = parser.parse_args()

    asset1 = args.token1
    asset2 = args.token2

    date_today = date.today()  # Exemple : 2024-03-01
    date_10d_ago = date_today - timedelta(days=10)

    print("date_today : ", date_today)
    print("date_10d_ago : ", date_10d_ago)

    period_id = '1DAY'  # Exemples : 1DAY, 1HRS, 30MIN
    time_start = '2024-04-10'  # Format : YYYY-MM-DD
    time_end = '2024-05-01'  # Format : YYYY-MM-DD

    df = coinapi_service.create_dataframe(coinapi_service.coin_api_get_exchange_rates(asset1, asset2, period_id))
    coinapi_service.plot_rates(df, asset1)


if __name__ == '__main__':
    main()
